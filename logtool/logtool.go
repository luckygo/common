package logtool

import (
	"errors"
	"github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"os"
	"time"
)

var (
	logLevel      logrus.Level
	logPath       string
	logNameRotate = "_%Y%m%d"
	logFormatter  = &logrus.JSONFormatter{TimestampFormat: "2006-01-02 15:04:05"}
)

func Init(ll logrus.Level, path string) {
	logLevel = ll
	logrus.SetLevel(logLevel)
	logrus.SetFormatter(logFormatter)
	logPath = path + "logs" + string(os.PathSeparator)
}

func IsRelease() bool {
	return logLevel != logrus.TraceLevel && logLevel != logrus.DebugLevel
}

func GetLogger(logName string) *logrus.Logger {
	if len(logPath) == 0 {
		panic(errors.New("LogTool not init"))
	}
	logWriter, err := rotatelogs.New(
		// 分割后的文件名称
		logPath+logName+logNameRotate+".log",
		// WithLinkName 为最新的日志建立软连接
		rotatelogs.WithLinkName(logPath+logName+".log"),
		// WithRotationTime 设置日志分割的时间
		rotatelogs.WithRotationTime(time.Hour*24),
		// WithMaxAge 设置文件清理前的最长保存时间
		// WithRotationCount 设置文件清理前最多保存的个数
		// WithMaxAge 和 WithRotationCount 二者只能设置一个，
		rotatelogs.WithRotationCount(60),
	)
	if err != nil {
		logrus.Errorf("config local file system for logger error: %v", err)
	}
	lfsHook := lfshook.NewHook(lfshook.WriterMap{
		logrus.InfoLevel:  logWriter,
		logrus.FatalLevel: logWriter,
		logrus.DebugLevel: logWriter,
		logrus.WarnLevel:  logWriter,
		logrus.ErrorLevel: logWriter,
		logrus.PanicLevel: logWriter,
	}, logFormatter)
	logger := logrus.New()
	logger.SetLevel(logLevel)
	logger.SetFormatter(logFormatter)
	logger.AddHook(lfsHook)
	return logger
}

func CheckError(logger *logrus.Logger, err error, msg string) bool {
	if err != nil {
		logger.Error(msg, err)
		return true
	}
	return false
}
