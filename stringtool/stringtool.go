package stringtool

import (
	"strings"
	"unicode"
)

func IsBlank(str string) bool {
	strLen := len(str)
	if str == "" || strLen == 0 {
		return true
	}
	for i := 0; i < strLen; i++ {
		if unicode.IsSpace(rune(str[i])) == false {
			return false
		}
	}
	return true
}

func IsNotBlank(str string) bool {
	return !IsBlank(str)
}

func DefaultIfBlank(str, def string) string {
	if IsBlank(str) {
		return def
	} else {
		return str
	}
}

func EqualsIgnoreCase(a, b string) bool {
	return a == b || strings.ToUpper(a) == strings.ToUpper(b)
}

func Substr(s string, start, length int) string {
	bt := []rune(s)
	if start < 0 {
		start = 0
	}
	if start > len(bt) {
		start = start % len(bt)
	}
	var end int
	if (start + length) > (len(bt) - 1) {
		end = len(bt)
	} else {
		end = start + length
	}
	return string(bt[start:end])
}

func RuneLen(s string) int {
	bt := []rune(s)
	return len(bt)
}

func GetSummary(s string, length int) string {
	summary := Substr(s, 0, length)
	if RuneLen(s) > length {
		summary += "..."
	}
	return summary
}

func GetBetweenStr(content, start, end string, addStart, addEnd bool) string {
	indexStart := strings.Index(content, start)
	if indexStart == -1 {
		return ""
	}
	indexStart += len(start)
	temp := ""
	if len(end) > 0 {
		indexEnd := strings.LastIndex(content[indexStart:], end)
		if indexEnd == -1 {
			return ""
		}
		temp = content[indexStart : indexEnd+indexStart]
	} else {
		temp = content[indexStart:]
	}
	if addStart {
		temp = start + temp
	}
	if addEnd {
		temp = temp + end
	}
	return temp
}
