package webtool

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/logger"
	"github.com/kataras/iris/v12/middleware/recover"
	"net/http"
	"os"
	"time"
)

func GetWebApp(logLevel string, logPath string) *iris.Application {
	app := iris.New()
	app.Use(recover.New())
	app.Configure(iris.WithOptimizations)
	app.Logger().SetLevel(logLevel)
	logFile := newLogFile(logPath)
	iris.RegisterOnInterrupt(func() {
		defer logFile.Close()
	})
	app.Logger().AddOutput(logFile)
	requestLogger := logger.New(logger.Config{
		Status: true,
		IP:     true,
		Method: true,
		Path:   true,
		Query:  true,
	})
	app.Use(requestLogger)
	app.OnAnyErrorCode(requestLogger, func(ctx iris.Context) {
		text := http.StatusText(ctx.GetStatusCode())
		_, _ = ctx.WriteString(text)
		ctx.Next()
	})
	return app
}

func todayFilename() string {
	today := time.Now().Format("20060102")
	return today + ".txt"
}

func newLogFile(path string) *os.File {
	filename := path + "web_" + todayFilename()
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	return f
}
