package tool

import (
	"github.com/mitchellh/go-homedir"
	"net"
	"os"
	"strconv"
	"time"
)

var (
	Separator = string(os.PathSeparator)
	HomePath  string
)

func init() {
	var err error
	HomePath, err = homedir.Dir()
	if err != nil {
		panic(err)
	}
}

func GetPath(name string) string {
	path := HomePath + Separator + name + Separator
	_, err := os.Stat(path)
	if err != nil {
		err := os.MkdirAll(path+"logs", os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
	return path
}

func One(port int) {
	listener, err := net.Listen("tcp", "127.0.0.1:"+strconv.Itoa(port))
	if err != nil {
		panic(err)
	}
	go func() {
		defer listener.Close()
		for {
			if conn, err := listener.Accept(); err != nil {
				conn.Close()
			}
		}
	}()
}

func Now() string {
	return time.Now().Format("2006-01-02 15:04:05")
}

func Timestamp() int64 {
	return time.Now().Unix()
}

func TimestampNano() int64 {
	return time.Now().UnixNano() / 1e6
}

func TimeFromTimestamp(unix int64) time.Time {
	return time.Unix(unix, 0)
}

func TimeFromTimestampNano(timestamp int64) time.Time {
	return time.Unix(0, timestamp*int64(time.Millisecond))
}

func TimestampFromTime(t time.Time) int64 {
	return t.UnixNano() / 1e6
}
