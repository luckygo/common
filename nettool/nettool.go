package nettool

import (
	"bytes"
	"encoding/binary"
	"errors"
	"github.com/miekg/dns"
	"math"
	"net"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const DefaultUserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36"

func Connect(address string, timeout time.Duration) bool {
	conn, err := net.DialTimeout("tcp", address, timeout)
	if err != nil {
		return false
	}
	defer conn.Close()
	return true
}

func IsIP(input string) bool {
	if len(input) == 0 {
		return false
	}
	return regexp.MustCompile(`^(?:(?:25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d\d?)$`).MatchString(input)
}

func IP2Int(ip string) uint32 {
	b := net.ParseIP(ip).To4()
	if b == nil {
		panic(errors.New("invalid ipv4 format"))
	}
	return uint32(b[3]) | uint32(b[2])<<8 | uint32(b[1])<<16 | uint32(b[0])<<24
}

func Int2IP(i uint32) string {
	if i > math.MaxUint32 {
		panic(errors.New("beyond the scope of ipv4"))
	}
	ip := make(net.IP, net.IPv4len)
	ip[0] = byte(i >> 24)
	ip[1] = byte(i >> 16)
	ip[2] = byte(i >> 8)
	ip[3] = byte(i)
	return ip.String()
}


func Proxies2Bytes(proxies []string) []byte {
	buffer := bytes.NewBuffer([]byte{})
	for _, i := range proxies {
		proxy := strings.Split(i, ":")
		intPort, _ := strconv.Atoi(proxy[1])
		_ = binary.Write(buffer, binary.BigEndian, IP2Int(proxy[0]))
		_ = binary.Write(buffer, binary.BigEndian, uint16(intPort))
	}
	return buffer.Bytes()
}

func Bytes2Proxies(data []byte) (proxies []string, err error) {
	db := bytes.NewBuffer(data)
	for i := 0; i < len(data)/6; i++ {
		var ip uint32
		var port uint16
		err = binary.Read(db, binary.BigEndian, &ip)
		if err != nil {
			return
		}
		err = binary.Read(db, binary.BigEndian, &port)
		if err != nil {
			return
		}
		proxies = append(proxies, Int2IP(ip)+":"+strconv.FormatInt(int64(port), 10))
	}
	return
}

func Resolve(host string) (ips []string, err error) {
	if len(host) == 0 {
		err = errors.New("nil host")
		return
	}
	if IsIP(host) {
		ips = []string{host}
		return
	}
	client := new(dns.Client)
	client.Timeout = 5 * time.Second
	msg := new(dns.Msg)
	msg.Id = dns.Id()
	msg.RecursionDesired = true
	msg.Question = []dns.Question{{dns.Fqdn(host), dns.TypeA, dns.ClassINET}}
	var rMsg *dns.Msg
	rMsg, _, err = client.Exchange(msg, "8.8.8.8:53")
	if err != nil {
		return
	}
	if len(rMsg.Answer) < 1 {
		err = errors.New("no answer")
		return
	}
	for _, answer := range rMsg.Answer {
		if a, ok := answer.(*dns.A); ok {
			ips = append(ips, a.A.String())
		}
	}
	return
}
