package httptool

import (
	"github.com/json-iterator/go"
	"strings"
	"testing"
)

func TestHttp(t *testing.T) {
	_, body, _ := DoRequest(Client, "GET", "http://httpbin.org/get").End()
	if !strings.Contains(body, "Mozilla") {
		t.Error(body)
	}
}

func TestCookie(t *testing.T) {
	_, body, _ := DoRequest(Client, "GET", "http://httpbin.org/cookies/set/aaa/bbb").End()
	if jsoniter.Get([]byte(body), "cookies", "aaa").ToString() != "bbb" {
		t.Error(body)
	}
}

func TestJson(t *testing.T) {
	output, _ := jsoniter.Marshal(map[string]string{"hello": "world"})
	if string(output) != "{\"hello\":\"world\"}" {
		t.Error()
	}
}
