package redistool

import (
	"errors"
	"fmt"
	redis2 "github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
	"gitlab.com/luckygo/common/jsontool"
	"gitlab.com/luckygo/common/logtool"
	"strconv"
	"time"
)

const (
	cachePrefix = "cache:"
	tempPrefix  = "temp:"
)

func NewRedisPool(host, password string, database int, timeout time.Duration, maxActive, maxIdle int, idleTimeout, maxConnLifetime time.Duration) (pool *redis2.Pool, err error) {
	redisPool := &redis2.Pool{
		MaxActive:       maxActive,
		MaxIdle:         maxIdle,
		IdleTimeout:     idleTimeout,
		MaxConnLifetime: maxConnLifetime,
		Wait:            true,
		Dial: func() (redis2.Conn, error) {
			con, err := redis2.Dial("tcp", host,
				redis2.DialPassword(password),
				redis2.DialDatabase(database),
				redis2.DialConnectTimeout(timeout),
				redis2.DialReadTimeout(timeout),
				redis2.DialWriteTimeout(timeout))
			if err != nil {
				return nil, err
			}
			return con, nil
		},
		TestOnBorrow: func(c redis2.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
	conn := redisPool.Get()
	err = conn.Err()
	_ = conn.Close()
	if err == nil {
		pool = redisPool
	}
	return
}

type RedisTool struct {
	redis   *redis2.Pool
	log     *logrus.Logger
	prefix  string
	expire  int
	isCache bool
}

func NewRedisTool(redis *redis2.Pool, log *logrus.Logger, isCache bool, name string, expire int) *RedisTool {
	prefix := cachePrefix
	if !isCache {
		prefix = tempPrefix
	}
	return &RedisTool{redis, log, fmt.Sprintf("%s%s:", prefix, name), expire, isCache}
}

func (r *RedisTool) SetExpire(key string, t int) error {
	conn := r.redis.Get()
	defer conn.Close()
	k := r.prefix + key
	result, err := redis2.Int(conn.Do("expire", k, t))
	if err != nil || result != 1 {
		if result != 1 {
			err = errors.New("code " + strconv.Itoa(result))
		}
		r.log.Error("redis set expire time failed: ", err)
	}
	return err
}

func (r *RedisTool) Get(key string, data interface{}) (suc bool, err error) {
	conn := r.redis.Get()
	defer conn.Close()
	k := r.prefix + key
	var tmp string
	tmp, err = redis2.String(conn.Do("get", k))
	if err != nil {
		if err == redis2.ErrNil {
			err = nil
		} else {
			logtool.CheckError(r.log, err, "redis get failed: ")
		}
		return
	}
	if r.isCache && r.expire > 0 {
		err = r.SetExpire(key, r.expire)
		if err != nil {
			return
		}
	}
	_, ok := data.(*int)
	if ok {
		*data.(*int), err = strconv.Atoi(tmp)
		if err != nil {
			err = errors.New("redis data parse failed: " + err.Error())
			return
		}
		return
	} else {
		_, ok = data.(*string)
		if ok {
			*data.(*string) = tmp
			return
		}
	}
	if len(tmp) < 2 {
		err = errors.New("redis data parse failed: " + tmp)
		return
	}
	err = jsontool.ParseJson(tmp, &data)
	if err != nil {
		err = errors.New("redis data parse failed: " + err.Error())
		return
	}
	suc = true
	return
}

func (r *RedisTool) Set(key string, data interface{}) (err error) {
	tmp, ok := data.(string)
	if !ok {
		tmp, err = jsontool.FormatJson(data)
		if err != nil {
			err = errors.New("redis date format failed: " + err.Error())
			return
		}
	}
	conn := r.redis.Get()
	defer conn.Close()
	k := r.prefix + key
	if ok && tmp == "i++" {
		_, err = conn.Do("incr", k)
		if logtool.CheckError(r.log, err, "redis incr failed: ") {
			return
		}
		if r.expire > 0 {
			err = r.SetExpire(key, r.expire)
		}
	} else {
		if r.expire > 0 {
			_, err = conn.Do("setex", k, r.expire, tmp)
		} else {
			_, err = conn.Do("set", k, tmp)
		}
		logtool.CheckError(r.log, err, "redis set failed: ")
	}
	return
}

func (r *RedisTool) Del(key string) (err error) {
	conn := r.redis.Get()
	defer conn.Close()
	_, err = conn.Do("del", r.prefix+key)
	logtool.CheckError(r.log, err, "redis delete failed: ")
	return
}
