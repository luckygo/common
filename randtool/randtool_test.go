package randtool

import (
	"fmt"
	"testing"
)

func TestRand(t *testing.T) {
	r := NewRandTool()
	fmt.Println("1", r.UserAgent(true))
	fmt.Println("2", r.UserAgent(false))
	fmt.Println("3", r.Name(true))
	fmt.Println("4", r.Name(false))
	fmt.Println("5", r.Province(true))
	fmt.Println("6", r.Province(false))
	fmt.Println("7", r.IP())
	fmt.Println("8", r.Str(0))
	fmt.Println("9", r.Str(66))
	fmt.Println("10", r.Num(0))
	fmt.Println("11", r.Num(66))
	fmt.Println("12", r.TEL())
	fmt.Println("13", r.QQ())
	fmt.Println("14", r.Mail())
	fmt.Println("15", r.ETH())
	fmt.Println("16", r.MD5())
}
