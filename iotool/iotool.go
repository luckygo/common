package iotool

import (
	"io/ioutil"
	"os"
	"strings"
)

func ReadLines(path string) (lines []string) {
	data, err := ioutil.ReadFile(path)
	if err == nil && len(data) > 1 {
		for _, line := range strings.Split(string(data), "\n") {
			if len(line) < 1 {
				continue
			}
			lines = append(lines, line)
		}
	}
	return
}

func WriteString(path string, content string, append bool) error {
	flag := os.O_RDWR | os.O_CREATE
	if append {
		flag = flag | os.O_APPEND
	} else {
		flag = flag | os.O_TRUNC
	}
	file, err := os.OpenFile(path, flag, 0644)
	if err != nil {
		return err
	}
	defer file.Close()
	_, err = file.WriteString(content)
	return err
}

func PathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}
