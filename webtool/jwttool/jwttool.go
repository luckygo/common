package jwttool

import (
	"errors"
	"github.com/square/go-jose/v3"
	"github.com/square/go-jose/v3/jwt"
	"time"
)

var (
	ErrMissing           = errors.New("token is missing")
	ErrExpired           = errors.New("token is expired (exp)")
	ErrNotValidYet       = errors.New("token not valid yet (nbf)")
	ErrIssuedInTheFuture = errors.New("token issued in the future (iat)")
)

type JWT struct {
	issuer   string
	audience string
	secret   interface{}
	maxAge   time.Duration
	signer   jose.Signer
}

func NEW(issuer, audience, signSecret string, maxAge time.Duration) (*JWT, error) {
	signer, err := jose.NewSigner(jose.SigningKey{
		Algorithm: jose.HS256,
		Key:       []byte(signSecret),
	}, (&jose.SignerOptions{}).WithType("JWT"))
	if err != nil {
		return nil, err
	}
	j := &JWT{
		issuer:   issuer,
		audience: audience,
		secret:   []byte(signSecret),
		maxAge:   maxAge,
		signer:   signer,
	}
	return j, nil
}

func (j *JWT) Token(subject string) (string, error) {
	now := time.Now()
	claims := &jwt.Claims{
		Issuer:   j.issuer,
		Subject:  subject,
		Audience: jwt.Audience{j.audience},
		IssuedAt: jwt.NewNumericDate(now),
		Expiry:   jwt.NewNumericDate(now.Add(j.maxAge)),
	}
	return jwt.Signed(j.signer).Claims(claims).CompactSerialize()
}

func (j *JWT) VerifyToken(token string) (subject string, err error) {
	if token == "" {
		err = ErrMissing
		return
	}
	var parsedToken *jwt.JSONWebToken
	parsedToken, err = jwt.ParseSigned(token)
	if err != nil {
		return
	}
	claims := &jwt.Claims{}
	err = parsedToken.Claims(j.secret, claims)
	if err != nil {
		return
	}
	err = claims.ValidateWithLeeway(jwt.Expected{Time: time.Now()}, 0)
	if err != nil {
		switch err {
		case jwt.ErrExpired:
			err = ErrExpired
		case jwt.ErrNotValidYet:
			err = ErrNotValidYet
		case jwt.ErrIssuedInTheFuture:
			err = ErrIssuedInTheFuture
		}
	}
	if err == nil && (claims.Issuer != j.issuer || claims.Audience[0] != j.audience) {
		err = jwt.ErrNotValidYet
	} else {
		subject = claims.Subject
	}
	return
}
