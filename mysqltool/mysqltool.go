package mysqltool

import (
	"database/sql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"time"
)

type Model struct {
	Id         uint64    `gorm:"primary_key;auto_increment" json:"id" form:"id"`
	CreateTime time.Time `gorm:"not null" json:"createTime" form:"createTime"`
	UpdateTime time.Time `gorm:"not null" json:"updateTime" form:"updateTime"`
}

func (m *Model) BeforeCreate() (err error) {
	m.CreateTime = time.Now()
	m.UpdateTime = time.Now()
	return
}

func (m *Model) BeforeUpdate() (err error) {
	m.UpdateTime = time.Now()
	return
}

type Paging struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
	Total int `json:"total"`
}

type PageResult struct {
	Page    *Paging     `json:"page"`
	Results interface{} `json:"results"`
}

func SqlNullString(value string) sql.NullString {
	return sql.NullString{
		String: value,
		Valid:  len(value) > 0,
	}
}

type CursorResult struct {
	Results interface{} `json:"results"`
	Cursor  string      `json:"cursor"`
}

func (p *Paging) Offset() int {
	offset := 0
	if p.Page > 0 {
		offset = (p.Page - 1) * p.Limit
	}
	return offset
}

func (p *Paging) TotalPage() int {
	if p.Total == 0 || p.Limit == 0 {
		return 0
	}
	totalPage := p.Total / p.Limit
	if p.Total%p.Limit > 0 {
		totalPage = totalPage + 1
	}
	return totalPage
}

func NewMySQLPool(dsn string, maxActive, maxIdle int, maxConnLifetime time.Duration, tablePrefix string) (pool *gorm.DB, err error) {
	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return tablePrefix + defaultTableName
	}
	var mysqlPool *gorm.DB
	mysqlPool, err = gorm.Open("mysql", dsn)
	if err != nil {
		return
	}
	mysqlPool.DB().SetMaxOpenConns(maxActive)
	mysqlPool.DB().SetMaxIdleConns(maxIdle)
	mysqlPool.DB().SetConnMaxLifetime(maxConnLifetime)
	mysqlPool.LogMode(true)
	mysqlPool.SingularTable(true)
	err = mysqlPool.DB().Ping()
	if err != nil {
		return
	}
	pool = mysqlPool
	return
}

func Tx(db *gorm.DB, txFunc func(tx *gorm.DB) error) (err error) {
	tx := db.Begin()
	if tx.Error != nil {
		return
	}
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			panic(r) // re-throw panic after Rollback
		} else if err != nil {
			tx.Rollback()
		} else {
			err = tx.Commit().Error
		}
	}()
	err = txFunc(tx)
	return err
}
