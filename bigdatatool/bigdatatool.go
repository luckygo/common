package bigdatatool

import (
	"github.com/PuerkitoBio/goquery"
	"strings"
)

func GetHtmlText(html string) string {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(html))
	if err != nil {
		return ""
	}
	return doc.Text()
}
