package randtool

import (
	uuid "github.com/iris-contrib/go.uuid"
	"gitlab.com/luckygo/common/nettool/urlencode"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type RandTool struct {
	uaList           []string
	nameListGBK      []string
	nameListUTF8     []string
	provinceListGBK  []string
	provinceListUtf8 []string
}

func Choice(array []string) string {
	return array[rand.Intn(len(array))]
}

func RandInt(min int, max int) int {
	return min + rand.Intn(max-min+1)
}

func Replace(raw, flag string, f func() string) string {
	n := strings.Count(raw, flag)
	if n > 0 {
		for i := 0; i < n; i++ {
			raw = strings.Replace(raw, flag, f(), 1)
		}
	}
	return raw
}

func NewRandTool() *RandTool {
	uaList := strings.Split(userAgents, "\n")
	nameListGBK := make([]string, 0)
	nameListUTF8 := make([]string, 0)
	for _, i := range strings.Split(names, "\n") {
		s, err := urlencode.Encode(i, "gbk")
		if err != nil {
			panic(err)
		}
		nameListGBK = append(nameListGBK, s)
		s, err = urlencode.Encode(i, "utf8")
		if err != nil {
			panic(err)
		}
		nameListUTF8 = append(nameListUTF8, s)
	}
	provinceListGBK := make([]string, 0)
	provinceListUtf8 := make([]string, 0)
	for _, i := range strings.Split(provinces, "\n") {
		s, err := urlencode.Encode(i, "gbk")
		if err != nil {
			panic(err)
		}
		provinceListGBK = append(provinceListGBK, s)
		s, err = urlencode.Encode(i, "utf8")
		if err != nil {
			panic(err)
		}
		provinceListUtf8 = append(provinceListUtf8, s)
	}
	return &RandTool{uaList, nameListGBK, nameListUTF8, provinceListGBK, provinceListUtf8}
}

func (r *RandTool) UUID() string {
	u, _ := uuid.NewV4()
	return strings.ReplaceAll(u.String(), "-", "")
}

func (r *RandTool) UserAgent(spider bool) string {
	if spider {
		return spiderUserAgent
	}
	return Choice(r.uaList)
}

func (r *RandTool) Name(gbk bool) string {
	if gbk {
		return Choice(r.nameListGBK)
	}
	return Choice(r.nameListUTF8)
}

func (r *RandTool) Province(gbk bool) string {
	if gbk {
		return Choice(r.provinceListGBK)
	}
	return Choice(r.provinceListUtf8)
}

func (r *RandTool) IP() string {
	var tmp []string
	for i := 0; i < 4; i++ {
		tmp = append(tmp, strconv.Itoa(RandInt(1, 254)))
	}
	return strings.Join(tmp, ".")
}

func (r *RandTool) Str(length int) string {
	if length == 0 {
		length = RandInt(5, 20)
	}
	tmp := ""
	for i := 0; i < length; i++ {
		tmp += string(Chars[rand.Intn(62)])
	}
	return tmp
}

func (r *RandTool) Num(length int) string {
	if length == 0 {
		length = 6
	}
	tmp := ""
	tmp += strconv.Itoa(rand.Intn(10))
	n := length - 2
	if n > 0 {
		for i := 0; i < n; i++ {
			tmp += strconv.Itoa(rand.Intn(10))
		}
	}
	tmp += strconv.Itoa(rand.Intn(10))
	return tmp
}

func (r *RandTool) TEL() string {
	return "1" + strconv.Itoa(RandInt(3, 8)) + r.Num(9)
}

func (r *RandTool) QQ() string {
	return r.Num(RandInt(8, 10))
}

func (r *RandTool) Mail() string {
	return r.QQ() + "@qq.com"
}

func (r *RandTool) ETH() string {
	chars := "abcdef0123456789"
	tmp := "0x"
	for i := 0; i < 40; i++ {
		tmp += string(chars[rand.Intn(16)])
	}
	return tmp
}

func (r *RandTool) MD5() string {
	chars := "abcdef0123456789"
	tmp := ""
	for i := 0; i < 32; i++ {
		tmp += string(chars[rand.Intn(16)])
	}
	return tmp
}

func (r *RandTool) Rand(raw string) string {
	if len(raw) == 0 {
		return ""
	}
	raw = Replace(raw, "%tm_ua%", func() string {
		return r.UserAgent(false)
	})
	raw = Replace(raw, "%tm_ip%", r.IP)
	raw = Replace(raw, "%tm_time%", func() string {
		return strconv.Itoa(int(time.Now().Unix()))
	})
	raw = Replace(raw, "%tm_name%", func() string {
		return r.Name(false)
	})
	raw = Replace(raw, "%tm_name_gbk%", func() string {
		return r.Name(true)
	})
	raw = Replace(raw, "%tm_prov%", func() string {
		return r.Province(false)
	})
	raw = Replace(raw, "%tm_prov_gbk%", func() string {
		return r.Province(true)
	})
	raw = Replace(raw, "%tm_str%", func() string {
		return r.Str(0)
	})
	raw = Replace(raw, "%tm_str2%", func() string {
		return r.Str(512)
	})
	raw = Replace(raw, "%tm_num%", func() string {
		return r.Num(0)
	})
	raw = Replace(raw, "%tm_qq%", r.QQ)
	raw = Replace(raw, "%tm_mail%", r.Mail)
	raw = Replace(raw, "%tm_tel%", r.TEL)
	raw = Replace(raw, "%tm_eth%", r.ETH)
	raw = Replace(raw, "%tm_md5%", r.MD5)
	return raw
}
