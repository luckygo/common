package httptool

import (
	"crypto/tls"
	"github.com/parnurzeal/gorequest"
	"time"
)

var (
	defaultHeaders = map[string]string{
		"User-Agent":    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36",
		"Accept":        "*/*",
		"Pragma":        "no-cache",
		"Cache-control": "no-cache",
	}
	tlsConfig = tls.Config{InsecureSkipVerify: true}
	Client    = GetHttpClientTimeout(30 * time.Second)
)

func GetHttpClient() *gorequest.SuperAgent {
	return gorequest.New().TLSClientConfig(&tlsConfig)
}

func GetHttpClientTimeout(timeout time.Duration) *gorequest.SuperAgent {
	return GetHttpClient().Timeout(timeout)
}

func DoRequest(client *gorequest.SuperAgent, method string, url string) *gorequest.SuperAgent {
	request := client.CustomMethod(method, url)
	for name := range defaultHeaders {
		request.Set(name, defaultHeaders[name])
	}
	return client
}
